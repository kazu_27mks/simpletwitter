package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	//つぶやきの登録
	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//メッセージ一覧の取得
	public List<UserMessage> select(String start, String end, String userId) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;

		try {
			connection = getConnection();

			String startDate;
			String endDate;

			if (!StringUtils.isEmpty(start)) {
				startDate = start + " 00:00:00";
			} else {
				startDate = " 2020-04-01 00:00:00";
			}

			if(!StringUtils.isEmpty(end)) {
				endDate = end + " 23:59:59";
			} else {
				Date date = new Date();
				SimpleDateFormat nowDate = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
				endDate = nowDate.format(date);
			}

			Integer id = null;
			if ((!StringUtils.isBlank(userId)) && (userId.matches("^[0-9]+$"))) {
				id = Integer.parseInt(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, id, LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//つぶやきの削除
	public void delete(Integer messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//編集するつぶやきの取得
	public Message select(Integer messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageId);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//つぶやきの変更機能
	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}

