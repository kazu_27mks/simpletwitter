package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	//返信の登録
	public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//返信一覧の取得
	public List<UserComment> select() {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserComment> comment = new UserCommentDao().select(connection, LIMIT_NUM);
			commit(connection);

			return comment;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}