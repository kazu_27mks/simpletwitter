package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;


@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String judgmentMessageId = request.getParameter("messageId");
		List<String> errorMessages = new ArrayList<String>();

		Message message = null;
		if (!(StringUtils.isBlank(judgmentMessageId)) && (judgmentMessageId.matches("^[0-9]+$"))) {
			Integer messageId = Integer.parseInt(judgmentMessageId);
			message = new MessageService().select(messageId);
		}

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

 		Message message = getMessage(request);

 		if (!isValid(message, errorMessages)) {
 			request.setAttribute("errorMessages", errorMessages);
 			request.setAttribute("message", message);
 			request.getRequestDispatcher("edit.jsp").forward(request, response);
 			return;
 		}
 		new MessageService().update(message);
		response.sendRedirect("./");
	}

 	private Message getMessage(HttpServletRequest request)
 			throws IOException, ServletException {

 		Message message = new Message();
 		message.setId(Integer.parseInt(request.getParameter("messageId")));
 		message.setText(request.getParameter("messageText"));
 		return message;
 	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();
		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}